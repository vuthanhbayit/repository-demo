﻿import axios from '../../core/plugins/http'
import CONSTANTS from '../../core/utils/constants';

export const logOut = ({commit}) => {
    return new Promise((resolve, reject) => {
        if (typeof (localStorage) !== 'undefined') {
            localStorage.removeItem(CONSTANTS.ACCESS_TOKEN);
            localStorage.removeItem(CONSTANTS.ACCESS_PERMISSION);
            localStorage.removeItem(CONSTANTS.CURRENT_USER);
        }
        return resolve();
    })
};
export const destroyToken = ({commit}) => {
    return new Promise((resolve, reject) => {
        axios.post('/logout').then(response => {
            localStorage.removeItem(CONSTANTS.ACCESS_TOKEN);
            localStorage.removeItem(CONSTANTS.ACCESS_PERMISSION);
            localStorage.removeItem(CONSTANTS.CURRENT_USER);
            context.commit('DESTROY_TOKEN');
            return resolve(response);
        }).catch(error => {
            localStorage.removeItem(CONSTANTS.ACCESS_TOKEN);
            localStorage.removeItem(CONSTANTS.ACCESS_PERMISSION);
            localStorage.removeItem(CONSTANTS.CURRENT_USER);
            context.commit('DESTROY_TOKEN');
            return reject(error);
        })
    })
};
export const retrieveToken = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/login',
                ...opts
            }
        }).then((response) => {
            const token = response.data.access_token;
            let permission = response.data.permission_role;
            commit('RETRIEVE_TOKEN', token);
            commit('PERMISSION', permission);
            commit("LOGGED_IN", response.data.user);
            if (typeof (localStorage) !== 'undefined') {
                localStorage.setItem(CONSTANTS.CURRENT_USER, JSON.stringify(response.data.user));
                localStorage.setItem(CONSTANTS.ACCESS_TOKEN, token);
                localStorage.setItem(CONSTANTS.ACCESS_PERMISSION, permission);
            }
            return resolve(response);
        }).catch(error => {
            commit("LOGGED_IN_ERROR");
            return reject(error)
        })
    })
};
export const saveUser = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/saveUser',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};
export const getDetailUser = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/detailUser',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};
export const deleteUser = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/deleteUser',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};
export const getListUser = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/listUser',
                ...opts
            }
        }).then(response => {
            commit('GET_LIST_USER', response.data);
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};

