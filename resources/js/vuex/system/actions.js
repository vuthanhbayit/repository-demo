﻿import axios from 'axios'

export const initSystem = ({ commit }) => {
    return new Promise((resolve, reject) => {
        return axios({
            method: 'post',
            url: '/api/system/init',
        }).then(response => {
            commit("INIT_SYSTEM", response);
            return resolve(response);
        }).catch(err => {
            commit("INIT_SYSTEM", null);
            return reject(err);
        });
    })
};
