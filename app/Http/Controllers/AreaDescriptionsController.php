<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\AreaDescriptionCreateRequest;
use App\Http\Requests\AreaDescriptionUpdateRequest;
use App\Repositories\AreaDescriptionRepository;
use App\Validators\AreaDescriptionValidator;

/**
 * Class AreaDescriptionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AreaDescriptionsController extends Controller
{
    /**
     * @var AreaDescriptionRepository
     */
    protected $repository;

    /**
     * @var AreaDescriptionValidator
     */
    protected $validator;

    /**
     * AreaDescriptionsController constructor.
     *
     * @param AreaDescriptionRepository $repository
     * @param AreaDescriptionValidator $validator
     */
    public function __construct(AreaDescriptionRepository $repository, AreaDescriptionValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $areaDescriptions = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $areaDescriptions,
            ]);
        }

        return view('areaDescriptions.index', compact('areaDescriptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AreaDescriptionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AreaDescriptionCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $areaDescription = $this->repository->create($request->all());

            $response = [
                'message' => 'AreaDescription created.',
                'data'    => $areaDescription->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $areaDescription = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $areaDescription,
            ]);
        }

        return view('areaDescriptions.show', compact('areaDescription'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $areaDescription = $this->repository->find($id);

        return view('areaDescriptions.edit', compact('areaDescription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AreaDescriptionUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AreaDescriptionUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $areaDescription = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'AreaDescription updated.',
                'data'    => $areaDescription->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'AreaDescription deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'AreaDescription deleted.');
    }
}
