<?php

namespace App\Http\Controllers;

use App\Criteria\ContactCriteria;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ContactCreateRequest;
use App\Http\Requests\ContactUpdateRequest;
use App\Repositories\ContactRepository;
use App\Validators\ContactValidator;

/**
 * Class ContactsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ContactsController extends Controller
{
    /**
     * @var ContactRepository
     */
    protected $repository;

    /**
     * @var ContactValidator
     */
    protected $validator;

    /**
     * ContactsController constructor.
     *
     * @param ContactRepository $repository
     * @param ContactValidator $validator
     */
    public function __construct(ContactRepository $repository, ContactValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contacts = $this->repository->scopeQuery(function ($query) use ($request) {
            return $query->where([
                ['fullName','like','%'.$request['name'].'%'],
                ['status','=',$request['status']]
            ]);
        })->paginate($request->per_page);

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $contacts,
                'success' => true
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $contact = $this->repository->find($request->id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $contact,
                'success' => true
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ContactUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ContactUpdateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            if ($request->id > 0) {
                $contact = $this->repository->update($request->all(), $request->id);
            } else {
                $contact = $this->repository->create($request->all());
            }

            $response = [
                'success' => true,
                'data'    => $contact->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()->first()
                ]);
            }
        }
    }

}
