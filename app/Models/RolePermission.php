<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Permission;

class RolePermission extends Model
{
    protected $table="permission_role";
    public function Permission()
    {
        return $this->hasOne(Permission::class, 'id', 'permission_id');
    }
}
