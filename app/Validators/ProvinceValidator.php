<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProvinceValidator.
 *
 * @package namespace App\Validators;
 */
class ProvinceValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
		'required'	=>'	area_id=>required',
	],
        ValidatorInterface::RULE_UPDATE => [
		'required'	=>'	area_id=>required',
		'required'	=>'	status=>required',
	],
    ];
}
