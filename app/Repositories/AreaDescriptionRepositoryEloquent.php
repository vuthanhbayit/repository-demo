<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AreaDescriptionRepository;
use App\Models\AreaDescription;
use App\Validators\AreaDescriptionValidator;

/**
 * Class AreaDescriptionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AreaDescriptionRepositoryEloquent extends BaseRepository implements AreaDescriptionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AreaDescription::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AreaDescriptionValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
