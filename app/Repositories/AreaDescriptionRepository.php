<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AreaDescriptionRepository.
 *
 * @package namespace App\Repositories;
 */
interface AreaDescriptionRepository extends RepositoryInterface
{
    //
}
